#!/usr/bin/env python
import subprocess

def main():
    command = 'id '
    sp = subprocess.Popen(command, stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE,
                          shell=True)
    out, err = sp.communicate()
    result = dict(errorcode=sp.returncode, stdout=out, stderr=err)
    if result['errorcode']==0:
        parts = result['stdout'].split(',')
        for part in parts:
            pieces = part.split('(')
            print pieces[1].replace(')','')


# ####################MAIN ENTRY POINT AS A SCRIPT ################
if __name__ == '__main__':
    main()
